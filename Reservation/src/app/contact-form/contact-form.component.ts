import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Contact} from '../model/contact';
import {ContactType} from '../model/contact-type';
import {ParamsService} from '../model/params-service';
import {NgbDateNativeAdapter} from '../shared/NgbDateNativeAdapter';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import { AppService } from '../services/appService';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  selectedContact: Contact = {
    contactType: {} as ContactType
  } as Contact;
  contactTypesList: ContactType[] = [];
  contactsList: Contact[] = [];
  callingService = false;
  serviceParams: ParamsService = {} as ParamsService;

  constructor(public router: Router,
    private datePickerServiceAdapter: NgbDateNativeAdapter,
    private appService: AppService,
    private toastService: ToastrService, private translate: TranslateService) {
}

  ngOnInit() {
    this.serviceParams.page = 1;
    this.callForContactsList();

    this.appService.getContactTypes().subscribe(
      res => this.contactTypesList = res,
      error => console.log(error)
    );
  }

  SubmitContactForm() {
    this.callingService = true;
    const bdate = this.datePickerServiceAdapter.toModel(this.selectedContact.birthdayPickerValue);
    this.selectedContact.birthDate = moment(bdate).utc().format('YYYY-MM-DDTHH:mm:ss-0300');

    this.appService.saveContact(this.selectedContact).pipe(
      finalize(() => this.callingService = false)).subscribe(
        (res: Contact) => {
          if (res) {
            const editedPos = this.contactsList.findIndex(r => r.contactId === res.contactId);
            if (editedPos >= 0) {
              this.contactsList[editedPos] = res;
            } else {
              this.contactsList.push(res);
              this.serviceParams.total +=1;
            }
            this.translate.get('savedContactOK').subscribe(
              translate => this.toastService.success(translate)
            );
            this.ResetSelectedContact();
          }
        },
        error => console.log(error)
      );
  }

  SelectContact(contact: Contact) {
    this.selectedContact = Object.assign({}, contact);
  }

  isSelect(contact: Contact) {
    return this.selectedContact.contactId === contact.contactId;
  }

  ResetSelectedContact() {
    this.selectedContact = {
      contactType: {} as ContactType
    } as Contact;
  }

  RemoveContact(id) {
    this.callingService = true;
    this.appService.removeContact(id).pipe(
      finalize(() => this.callingService = false)).subscribe(
      (res: boolean) => {
        if (res) {
            const editedPos = this.contactsList.findIndex(r => r.contactId === id);
            this.contactsList.splice(editedPos, 1);
            this.serviceParams.total = this.serviceParams.total -1;
            this.selectedContact = this.contactsList[this.contactsList.length] || {
              contactType: {} as ContactType
            } as Contact;
           this.translate.get('removedContactOK').subscribe(
              translate => this.toastService.success(translate)
            );
        }
      },
      error => console.log(error)
    );
  }

  ChangeContactType(event) {
  this.selectedContact.contactType = event || {} ;
  }

  ExcuteFilter(property, isAsc) {
    this.serviceParams.filter = property;
    this.serviceParams.sort = (isAsc ? 'asc' : 'desc');
    this.callForContactsList();
  }

  PageChage(event) {
    this.serviceParams.page = event;
    this.serviceParams.count = 10;
    this.callForContactsList();
  }

  private callForContactsList() {
    this.callingService = true;
    this.appService.getContacts(this.serviceParams).pipe(
      finalize(() => this.callingService = false)).subscribe(
        (res: any) => {
          if (res != null && res.contactList && res.contactList.length > 0) {
            this.contactsList = res.contactList;
            this.contactsList.map(c => c.birthdayPickerValue = this.datePickerServiceAdapter.fromModel(new Date(c.birthDate)));
            this.serviceParams.total = res.contactCount;
            this.selectedContact = this.contactsList[0];
          }
        },
        error => console.log(error)
      );
  }

}

