import { Component, OnInit } from '@angular/core';
import {Reservation} from '../model/reservation';
import { AppService } from '../services/appService';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {ParamsService} from '../model/params-service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {

  sortFilters = [];
  reservations: Reservation[] = [];
  selectedReservation: Reservation = {} as Reservation;
  callingService = false;
  serviceParams: ParamsService = {} as ParamsService;

  constructor(private translate: TranslateService,
              public router: Router,
              private modalService: NgbModal,
              private appService: AppService,
              private toastService: ToastrService) {

  }

  ngOnInit() {
    this.serviceParams.page = 1;
    this.translate.onLangChange.subscribe(l => {
      this.setLangOfFiltersValues();
    });
    this.setLangOfFiltersValues();
    this.callForReservations();
  }

  private callForReservations() {
    this.callingService = true;
    this.appService.getReservations(this.serviceParams).pipe(
      finalize(() => this.callingService = false)).subscribe(
      (res: any) => {
        if (res != null && res.reservationList && res.reservationList.length > 0) {
          this.serviceParams.total = res.reservationCount;
          this.reservations = res.reservationList;
        }
      },
      error => {
        console.log(error);
      }
    );
    this.callingService = false;
  }

  FilterChange(e) {
    if (e) {
      this.serviceParams.filter = e.property;
      this.serviceParams.sort = e.direction;
    }
    this.callForReservations();
  }

  setLangOfFiltersValues() {
    this.sortFilters = [];
    this.translate.get('alphAsc').subscribe(t => this.addFilterToList('destination', t, 'asc'));
    this.translate.get('alphDesc').subscribe(t => this.addFilterToList('destination', t, 'desc'));
    this.translate.get('dateAsc').subscribe(t => this.addFilterToList('date', t, 'asc'));
    this.translate.get('dateDesc').subscribe(t => this.addFilterToList('date', t, 'desc'));
    this.translate.get('rankingAsc').subscribe(t => this.addFilterToList('rating', t, 'asc'));
    this.translate.get('rankingDesc').subscribe(t => this.addFilterToList('rating', t, 'desc'));
  }

  /** add or refresh the language of the filter property **/
  private addFilterToList(prop: string, traduction: string, direction: string) {
    const filterObj = this.sortFilters.find(f => f.property === prop && f.direction === direction);
    if (filterObj) {
      filterObj.name = traduction;
    } else {
      this.sortFilters.push(
        {
          name: traduction,
          direction: direction,
          property: prop
        }
      );
    }
    this.sortFilters = Object.assign([], this.sortFilters);
  }

  SelectReservation(reservation: Reservation, reservationEditModal) {
    this.selectedReservation = Object.assign({}, reservation);
    this.modalService.open(reservationEditModal, {size: 'lg', centered: true});
  }

  PageChage(event) {
    this.serviceParams.page = event;
    this.serviceParams.count = 10;
    this.callForReservations();
  }

  SetFavorite(reservation: Reservation) {
    reservation.isFavorite = !reservation.isFavorite;
    this.selectedReservation = reservation;
    this.saveReservation();
  }

  SubmitReservationForm(closeModal) {
    this.saveReservation(closeModal);
  }

  private saveReservation(closeModal?) {
    this.callingService = true;
    this.appService.saveReservation(this.selectedReservation).pipe(
      finalize(() => this.callingService = false)).subscribe(
      (res: Reservation) => {
        if (res) {
          const editedPos = this.reservations.findIndex(r => r.reservationId === res.reservationId);
          this.reservations[editedPos] = res;
          this.translate.get('savedReservationOK').subscribe(
            translate => this.toastService.success(translate)
          );
        }
        if (closeModal) {
          closeModal();
        }
      },
      error => console.log(error)
    );

    this.callingService = false;
  }

}