import {Component, Injectable, OnInit} from '@angular/core';
import {Contact} from '../model/contact';
import {ContactType} from '../model/contact-type';
import {Reservation} from '../model/reservation';
import {Router} from '@angular/router';
import {ParamsService} from '../model/params-service';
import { finalize } from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NgbDateNativeAdapter} from '../shared/NgbDateNativeAdapter';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import { AppService } from '../services/appService';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
  
  selectedContact: Contact = {
    contactType: {} as ContactType,
    reservations: []
  } as Contact;
  
  selectedReservation: Reservation = {isFavorite: false} as Reservation;
  contactTypesList: ContactType[] = [];
  contactsList: Contact[] = [];
  contactListisCollapsed = true;
  callingService = false;
  serviceParams: ParamsService = {page: 0} as ParamsService;
  private searchTermStream = new Subject<string>();

  public search(term: string) {
    this.serviceParams.filter = term;
    this.searchTermStream.next(term);
  }

  constructor(public router: Router,
              private appService: AppService,
              private datePickerServiceAdapter: NgbDateNativeAdapter,
              private toastService: ToastrService, private translate: TranslateService) { }

  ngOnInit() {
    this.serviceParams.page = 1;
    this.appService.getContactTypes().subscribe(
      res => this.contactTypesList = res,
      error => console.log(error)
    );

    this.callForContactsList();

    const displayDataChanges = [
      this.searchTermStream,
      this.serviceParams.page
    ];
  }

  PageChage(event) {
    this.serviceParams.page = event;
    this.serviceParams.count = 10;
    this.callForContactsList();
  }

  private callForContactsList(mode?) {
    this.callingService = true;
    this.appService.getContacts(this.serviceParams).pipe(
      finalize(() => this.callingService = false)).subscribe(
      (res: any) => {
          if (res.contactList || res.contactList.length > 0) {
            this.contactsList = res.contactList;
            this.contactsList.map(c => c.birthdayPickerValue = this.datePickerServiceAdapter.fromModel(new Date(c.birthDate)));
            this.serviceParams.total = res.contactCount;
          }
        },
        error => console.log(error)
      );
  }

  SubmitContactReservationForm() {
    this.callingService = true;
    
    this.selectedReservation.creationDate = moment(new Date()).utc().format('YYYY-MM-DDTHH:mm:ss-0300');
    this.selectedReservation.contactId = this.selectedContact.contactId;
    this.selectedContact.reservations.push(this.selectedReservation);

    const bdate = this.datePickerServiceAdapter.toModel(this.selectedContact.birthdayPickerValue);
    this.selectedContact.birthDate = moment(bdate).utc().format('YYYY-MM-DDTHH:mm:ss-0300');
    this.appService.saveContact(this.selectedContact).pipe(
      finalize(() => this.callingService = false)).subscribe(
        (res: Contact) => {
          if (res) {
            const editedPos = this.contactsList.findIndex(r => r.contactId === res.contactId);
            if (editedPos >= 0) {
              this.contactsList[editedPos] = res;
            } else {
              this.contactsList.push(res);
              this.serviceParams.total =+1;
            }
             this.translate.get('savedContactOK').subscribe(
              translate => this.toastService.success(translate)
            ); 
          }
        },
        error => console.log(error)
      );
  }

  SelectContact(contact) {
    this.selectedContact = contact || {
      contactType: {} as ContactType,
      reservations: []
    } as Contact;
  }

  ChangeContactType(event) {
    this.selectedContact.contactType = event || {} ;
  }

}
