import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import { AppService } from './services/appService';
import { ContactFormComponent } from './contact-form/contact-form.component';
import {NgSelectModule} from '@ng-select/ng-select';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {FormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {NgbDateNativeAdapter} from './shared/NgbDateNativeAdapter';
import {
  NgbCollapseModule, NgbDateAdapter, NgbDatepickerModule, NgbModalModule,
  NgbPaginationModule
} from '@ng-bootstrap/ng-bootstrap';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { BarRatingModule } from "ngx-bar-rating";
import { ReservationComponent } from './reservation/reservation.component';
import {ErrorService} from './services/error-service.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    ReservationListComponent,
    ContactFormComponent,
    ReservationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    NgSelectModule,
    FormsModule,
    NgbDatepickerModule,
    NgbCollapseModule,
    NgbModalModule,
    NgbPaginationModule,
    BarRatingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot(
      {
        timeOut: 3000,
        preventDuplicates: true,
        closeButton: true
      }
    )
  ],
  providers: [
    AppService,
    NgbDateNativeAdapter, 
    ErrorService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
