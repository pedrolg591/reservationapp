import { Injectable} from '@angular/core';
import { Reservation } from '../model/reservation';
import { Contact } from '../model/contact';
import { ContactType } from '../model/contact-type';
import {ParamsService} from '../model/params-service';
import {ReservationResponse} from '../model/reservation-response';
import {catchError} from 'rxjs/operators';
import {ErrorService} from './error-service.service';
import {Observable, from} from 'rxjs';
import {of} from 'rxjs'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {server} from '../app.config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const serverRest = environment.production ? server.production : server.development;

@Injectable({
    providedIn: 'root'
  })

  export class AppService {

    constructor(private http: HttpClient,
                private errorService: ErrorService) { }

    getContactTypes(): Observable<any> {
      const url = serverRest + '/Contact/ContactTypes';
      return this.getAll(url, 'getContactsTypes');
    }

   /**
   * List all reservations
   * @returns {Observable<Object>}
   */
    getReservations(params: ParamsService): Observable<any> 
    {
      const url = serverRest + '/Reservation/ReservationList';
      return this.http.post(url, params, this.setHeader())
      .pipe(
        catchError(this.handleError('getReservations', null))
      );
    }

    saveReservation(reservation: Reservation) {
      const url = serverRest + '/Reservation/Update';
      return this.http.put<Reservation>(url, JSON.stringify(reservation), this.setHeader())
        .pipe(
          catchError(this.handleError('saveReservation', null))
        );
    }

     /**
   * List all contacts
   * @returns {Observable<Object>}
   */
    getContacts(params: ParamsService): Observable<any> 
    {
      const url = serverRest + '/Contact/ContactList';
      return this.http.post(url, params, this.setHeader())
      .pipe(
        catchError(this.handleError('getContacts', null))
      );
    }

    saveContact(contact: Contact) {
      const url = serverRest + '/Contact/Update';
      return this.http.put<Contact>(url, JSON.stringify(contact), this.setHeader())
        .pipe(
          catchError(this.handleError('saveContact', null))
        );
    }

    removeContact(id) {
      const url = serverRest + '/Contact';
      return this.http.delete(url + '/' + id, this.setHeader())
        .pipe(
          catchError(this.handleError('removeContact', {}))
        );
    }

    private getAll(url, functionName, params?) {
      const options = this.setHeader();
      options['params'] = params || {};
      return this.http.get(url, options)
        .pipe(
          catchError(this.handleError(functionName, []))
        );
    }

    private setHeader() {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json;charset=utf-8',
          'Accept': 'application/json'
        })
      };
      return httpOptions;
    }

     /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      this.errorService.extracted(error);

      // TODO: send the error to remote logging infrastructure
      console.log(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);

    };
  }

  }