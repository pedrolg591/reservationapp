export interface ParamsService {
    sort: string;
    page: number;
    count: number;
    filter: string;
    total: number;
  }