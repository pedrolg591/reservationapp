export interface Reservation {
    reservationId: number;
    destination: string;
    creationDate: string;
    ranking: number;
    isFavorite: boolean;
    description: string;
    contactId: number;
  }