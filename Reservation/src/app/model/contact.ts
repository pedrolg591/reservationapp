import {ContactType} from './contact-type';
import {Reservation} from './reservation';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

export interface Contact {
  contactId?: number;
  name?: string;
  birthDate?: string;
  birthdayPickerValue?: NgbDateStruct;
  phone?: string;
  contactType?: ContactType;
  reservations?: Reservation[];
}
