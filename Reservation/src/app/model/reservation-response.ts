import { Reservation } from "./reservation";

export class ReservationResponse{
    
    constructor(public reservations:Reservation[], public count:number){

    }
}