import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactFormComponent } from './contact-form/contact-form.component';
import {ReservationListComponent} from './reservation-list/reservation-list.component'
import {ReservationComponent} from './reservation/reservation.component'
import { from } from 'rxjs';

const routes: Routes = [
  {path: '', redirectTo: '/reservationList', pathMatch: 'full'},
  {path: 'reservationList', component: ReservationListComponent},
  {path: 'contactForm', component: ContactFormComponent},
  {path: 'reservation', component: ReservationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
